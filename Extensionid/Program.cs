﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensionid
{

    static class E
    {
        public static string Trüki(this string s)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}: {s}");
            return s;
        }

        //public static int Summa(this IEnumerable<int> mass)
        //{
        //    int summa = 0;
        //    foreach (var x in mass) summa += x;
        //    return summa;
        //}

        public static IEnumerable<int> Paaris (this IEnumerable<int> mass)
        {
            foreach(var x in mass)
            {
                if (x % 2 == 0) yield return x;
            }
        } 

        //public static IEnumerable<int> 
        //    Millised(this IEnumerable<int> mass, Func<int,bool> f)
        //{
        //    foreach (var x in mass)
        //        if (f(x)) yield return x;
        //}

        //public static bool KasPaaritu(int x) => x % 2 != 0;
        

    }

    class Program
    {
        static void Main(string[] args)
        {
            if (false)
            {
                Test t = new Test();
                t
                    .Meetod1()
                    .Meetod2()
                    .Meetod1()
                    ;
                Test.Meetod1(Test.Meetod2(Test.Meetod1(t)));

                Console.WriteLine(
                    "Henn on tore poiss"   // see on string
                    .Replace("tore", "kole")   // teeme asenduse
                    .Split(' ')     // tükeldab
                    [2]    // võtab kolmanda
                           //.ToUpper() // teeb suureks
                );

                //E.Trüki("Algus");

                //E.Trüki("Lõpp");

                "Algus".Trüki();
                "Henn on tore poiss"
                    .Trüki()
                    .Split(' ')[0]
                    .ToUpper()
                    .Trüki()
                    .ToLower()
                    .Trüki()
                    ;
            }

            List<int> arvud = new List<int> { 1, 2, 3, 4, 5, 6, 7 };




            Console.WriteLine(arvud
                .Where(x => x % 3 == 0)
                .Sum());
  
            

        }
    }

    class Test
    {
        // pole tähtis millest ta koosneb

        public Test Meetod1()  // instantsi meetod
        {
            Console.WriteLine("i1: " + this.ToString());
            return this;
        }
        public Test Meetod2()  // instantsi meetod
        {
            Console.WriteLine("i2: " + this.ToString());
            return this;
        }



        // staatiline meetod instantsi parameetriga
        public static Test Meetod1(Test x)
        {
            Console.WriteLine("s1: " + x.ToString());
            return x;
        }
        public static Test Meetod2(Test x)
        {
            Console.WriteLine("s2: " + x.ToString());
            return x;
        }

    }

}
