﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelExtensione
{
    static class Program
    {
        static IEnumerable<T> Trüki<T>(this IEnumerable<T> coll, string pealkiri="")
        {
            if (pealkiri != "") Console.WriteLine("\n"+pealkiri);
            foreach (var x in coll) Console.WriteLine(x);
            return coll;
        }

        static void Main(string[] args)
        {
            int[] arvud1 = { 1, 3, 5, 7, 9 };
            double[] arvud2 = { 1, 4, 6, 8, 10, 12 };

            arvud1.Union(arvud2.OfType<int>()).Trüki("Union:"); // korjab topeltväärtused välja
//            arvud1.Concat(arvud2).Trüki("Concat:"); // paneb kaks asja muutmata kokku

            arvud2.Zip(arvud1, (x, y) => x * y).Trüki("korrutis");

            var q = arvud1
                .Zip(arvud2, (x, y) => new { Vasakul = x, Paremal = y, Summa = x + y })
            ;
            q.Trüki("MIs see nüüd oli:");

            var q1 = from x in q
                     where x.Summa < 10
                     select x;
            q1.Trüki("miskijaburasi");

            arvud1.Union(arvud2.OfType<int>())
                
                .OrderBy(x => x % 3)
                .ThenBy(x => x)
                .Reverse()
                .Trüki("sorteeritud");

        }
    }
}
